angular.module('myApp', [])

.controller('loginController', function($scope, $http, $location) {
  $scope.login = function () {
		// body...
		$scope.return_data = '';
		if($scope.loginForm.$valid){
			console.log($scope.email);
			console.log($scope.password);
			$http.post('http://localhost/teachme-webserver/index.php/api/Users/login',{email:$scope.email, password:$scope.password}).success(function(data){
				$scope.return_data = data;
				console.log(data);
				if($scope.return_data.Status == true){
					localStorage.setItem('userid', $scope.return_data.user.ID);
					var lat;
					var longi;
					navigator.geolocation.watchPosition(showPosition);
					function showPosition(pos){
						 lat = pos.coords.latitude;
						 longi = pos.coords.longitude;
						 console.log(lat + " " + longi);
							$http.post('http://localhost/teachme-webserver/index.php/api/Users/updateLocation',{userID:localStorage.getItem('userid'), longitude:longi, latitude:lat}).success(function(data){
								console.log(data);
						});
					}

					setTimeout(function(){ window.location.assign("home.html"); }, 2000);
					
	
				}else{
					 $scope.wrongCredentials = true;
				}
				
			});
		
		}
	}

})

.controller('gigsController', function($scope, $http, $location) {
	$scope.getFeed = function () {  
		var lat;
		var longi;
			navigator.geolocation.watchPosition(showPosition);
			function showPosition(pos){
				 lat = pos.coords.latitude;
				 longi = pos.coords.longitude;

				 $http.get('http://localhost/teachme-webserver/index.php/api/Gig/getGigFeed/'+ lat +'/' + longi).success(function(data){
					$scope.gigs = data.Gigs;
					console.log(data.Gigs);
				});
			}

	}

	$scope.getAddress = function () {  
		var lat;
		var longi;
			navigator.geolocation.watchPosition(showPosition);
			function showPosition(pos){
				 lat = pos.coords.latitude;
				 longi = pos.coords.longitude;

				 $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat +'%2C'+ longi+ '&sensor=true').success(function(data){
					$scope.mylocation = data.results[3].formatted_address;
				});
			}

	}

	$scope.view_gig = function (gigid){
		localStorage.setItem('gigid', gigid);
		window.location.assign("viewgig.html")
	}

	$scope.getGig = function (){
		$http.get('http://localhost/teachme-webserver/index.php/api/Gig/getGig/'+ localStorage.getItem('gigid'))
		.success(function(data){
			$scope.gigdata = data.Gig;
			console.log($scope.gigdata);
		});

		
		$http.get('http://localhost/teachme-webserver/index.php/api/Gig/getGigComments/'+ localStorage.getItem('gigid'))
		.success(function(data){
			$scope.commentdata = data.Comments;
			console.log($scope.commentdata);
			
		});
	}

	$scope.postComment = function () {
		// body...
		$scope.return_data = '';
		if($scope.commentForm.$valid){
			$http.post('http://localhost/teachme-webserver/index.php/api/Gig/addComment',{postID:localStorage.getItem('gigid'), userID:localStorage.getItem('userid'),comment:$scope.comment, commentType:1}).success(function(data){
				$scope.return_data = data;
				console.log(data);
				if($scope.return_data.Status == true){
					location.reload();
	
				}else{
					 $scope.wrongCredentials = true;
				}
				
			});
		
		}
	}

})

.controller('userController', function($scope, $http, $location) {
  $scope.viewProfile = function () {
		$http.get('http://localhost/teachme-webserver/index.php/api/Users/getUser/'+ localStorage.getItem('userid'))
		.success(function(data){
			$scope.userprofile = data.User;
			console.log($scope.userprofile);
			
		});

	}

})